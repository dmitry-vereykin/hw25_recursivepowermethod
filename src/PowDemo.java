/**
 * Created by Dmitry Vereykin on 7/22/2015.
 */
public class PowDemo {
    public static void main(String[] args) {
        double x = 2, y = 10;
        System.out.println(x + " raised to the power of " + y + " is " + pow(x, y));
    }

    public static double pow(double x , double y) {
        if (y > 0)
            return x * pow(x, y - 1);
        else
            return 1;
    }
}
